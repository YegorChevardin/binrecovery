# BinRecover

An application that helps to recover files from the rubbish bin. (From any folder to any folder :3)

## 1. Execution

### 1.1 Needed elements for execution
- To exectue the application you need to install [jdk](https://www.oracle.com/java/technologies/javase/jdk18-archive-downloads.html) at least `18` version.
- For execution just pass this command in terminal: `java -jar BirRecover.jar`
- Or if you want to execute the application from the IDE - you are welcome, it will work properly, just do not miss the part with jdk instalation.

###### (C) Yegor Chevardin all rights reserved.