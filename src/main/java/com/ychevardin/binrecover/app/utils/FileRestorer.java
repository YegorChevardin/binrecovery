package com.ychevardin.binrecover.app.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;

public class FileRestorer {
    private FileRestorer() {}

    public static void restore(FileInputStream inputFile, String outputDirectoryPath, String fileName) throws IOException {
        byte[] inputFileBytes = inputFile.readAllBytes();

        try (FileOutputStream outputFile = new FileOutputStream(outputDirectoryPath + FileSystems.getDefault().getSeparator() + fileName)) {
            outputFile.write(inputFileBytes);
        }
    }
}
