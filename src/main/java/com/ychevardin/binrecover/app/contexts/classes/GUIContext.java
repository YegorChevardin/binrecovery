package com.ychevardin.binrecover.app.contexts.classes;

import com.ychevardin.binrecover.app.utils.FileRestorer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.*;

public class GUIContext implements UIContext {
    private static GUIContext instance = null;
    private String rubbishBinPath;
    private String fileName;
    private final Scanner scanner = new Scanner(System.in);
    private FileInputStream chosenFile = null;
    private String fileOutputDir;

    private GUIContext() {}

    public static GUIContext getInstance() {
        if (instance == null) {
            instance = new GUIContext();
        }
        return instance;
    }

    @Override
    public void run() throws FileNotFoundException {
        getRubbishBinPath();
        System.out.println("------------------");
        System.out.println("Selected : " + fileName);
        System.out.println("Rubbish bin directory: " + rubbishBinPath);
        System.out.println("------------------");
        System.out.println("Please, select place where you want to recover the file");
        getOutputDirectory();
        System.out.println("Selected output file directory: " + fileOutputDir);

        if (proceed()) {
            System.out.println("File restored successfully!");
        } else {
            System.out.println("Could not restore this file from selected directory.");
        }
        System.exit(0);
    }

    @Override
    public String getRubbishBinPath() throws FileNotFoundException {
        fileName = getFileName();
        return rubbishBinPath;
    }

    @Override
    public String getFileName() throws FileNotFoundException {
        try {
            JFileChooser currentFileChooser = chooseFile();
            chosenFile = new FileInputStream(currentFileChooser.getSelectedFile().getAbsolutePath());
            rubbishBinPath = currentFileChooser.getCurrentDirectory().getAbsolutePath();

            return currentFileChooser.getSelectedFile().getName();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No file selected, please try again!");
        }
    }

    @Override
    public String getOutputDirectory() {
        try {
            JFileChooser currentFileChooser = chooseDirectory();
            fileOutputDir = currentFileChooser.getSelectedFile().getAbsolutePath();

            return fileOutputDir;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No file selected, please try again!");
        }
    }

    @Override
    public boolean proceed() {
        try {
            FileRestorer.restore(chosenFile, fileOutputDir, fileName);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private JFileChooser chooseFile() {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int response = jFileChooser.showOpenDialog(new JFrame("Recovering file selection"));

        if (response != JFileChooser.APPROVE_OPTION) {
            throw new NullPointerException();
        }
        return jFileChooser;
    }

    private JFileChooser chooseDirectory() {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int response = jFileChooser.showOpenDialog(new JFrame("Directory to recover selection"));

        if (response != JFileChooser.APPROVE_OPTION) {
            throw new NullPointerException();
        }
        return jFileChooser;
    }
}
