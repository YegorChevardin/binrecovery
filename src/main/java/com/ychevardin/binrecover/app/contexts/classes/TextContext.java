package com.ychevardin.binrecover.app.contexts.classes;

import com.ychevardin.binrecover.app.utils.FileRestorer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class TextContext implements UIContext {
    private static TextContext instance;
    private String fileName;
    private String rubbishBinPath;
    private final Scanner scanner = new Scanner(System.in);
    private FileInputStream chosenFile = null;
    private String fileOutputDir;

    private TextContext() {}

    public static TextContext getInstance() {
        if (instance == null) {
            instance = new TextContext();
        }
        return instance;
    }

    @Override
    public void run() throws FileNotFoundException {
        System.out.println("------------------");
        System.out.println("Please, type here a absolute path for file you want to restore: ");
        getRubbishBinPath();
        System.out.println("Please, type here new name for file you want to restore: ");
        getFileName();
        System.out.println("------------------");
        System.out.println("Selected rubbish bin path: " + rubbishBinPath);
        System.out.println("New file name: " + fileName);
        System.out.println("------------------");
        System.out.println("Please, type here the absolute path to the directory where you want to recover the file");
        getOutputDirectory();
        System.out.println("------------------");
        System.out.println("Selected output file directory: " + fileOutputDir);
        System.out.println("------------------");
        if (proceed()) {
            System.out.println("File restored successfully!");
        } else {
            System.out.println("Could not restore this file from selected directory.");
        }
        System.exit(0);
    }

    @Override
    public String getRubbishBinPath() throws FileNotFoundException {
        rubbishBinPath = scanner.nextLine();
        chosenFile = new FileInputStream(rubbishBinPath);
        return rubbishBinPath;
    }

    @Override
    public String getFileName() {
        fileName = scanner.nextLine();
        return fileName;
    }

    @Override
    public String getOutputDirectory() {
        fileOutputDir = scanner.nextLine();

        return fileOutputDir;
    }

    @Override
    public boolean proceed() {
        try {
            FileRestorer.restore(chosenFile, fileOutputDir, fileName);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
