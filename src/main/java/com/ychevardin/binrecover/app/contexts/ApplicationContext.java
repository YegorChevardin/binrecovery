package com.ychevardin.binrecover.app.contexts;

import com.ychevardin.binrecover.app.contexts.classes.GUIContext;
import com.ychevardin.binrecover.app.contexts.classes.TextContext;

import java.util.Scanner;

public class ApplicationContext {
    private static ApplicationContext instance = null;
    private final Scanner scanner = new Scanner(System.in);

    private ApplicationContext() {}

    public static ApplicationContext getInstance() {
        if (instance == null) {
            instance = new ApplicationContext();
        }
        return instance;
    }

    public void start() {
        try {
            printLogo();
            showMenu();

            switch (typeInt()) {
                case 0 -> System.exit(0);
                case 1 -> TextContext.getInstance().run();
                case 2 -> GUIContext.getInstance().run();
                case -1, default -> {
                    System.out.println("You typed wrong value. Please, try again!");
                    start();
                }
            }
        } catch (Exception e) {
            printException(e);
            System.exit(0);
        }
    }

    private int typeInt() {
        int result = -1;

        try {
            result = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException ignored) {}

        return result;
    }

    private void showMenu() {
        System.out.println("------------------");
        System.out.println("'0' - exit the application");
        System.out.println("'1' - text journey");
        System.out.println("'2' - GUI journey (select file in window to recover)");
        System.out.println("* Note that, if you will pass unexpected input the application will restart itself");
        System.out.println("------------------");
    }

    private void printLogo() {
        System.out.println(
                """
                         ___ _      ___                       \s
                        | _ |_)_ _ | _ \\___ __ _____ _____ _ _\s
                        | _ \\ | ' \\|   / -_) _/ _ \\ V / -_) '_|
                        |___/_|_||_|_|_\\___\\__\\___/\\_/\\___|_|""".indent(1));
    }

    private void printException(Exception exception) {
        System.out.println("------------------");
        System.out.println("Some error occur!");
        System.out.println("Message:");
        System.out.println(exception.getMessage());
        System.out.println("------------------");
    }
}
