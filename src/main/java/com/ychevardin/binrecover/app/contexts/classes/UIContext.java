package com.ychevardin.binrecover.app.contexts.classes;

import java.io.FileNotFoundException;

public interface UIContext {
    public void run() throws FileNotFoundException;
    public String getRubbishBinPath() throws FileNotFoundException;
    public String getFileName() throws FileNotFoundException;
    public String getOutputDirectory() throws FileNotFoundException;
    public boolean proceed();
}
