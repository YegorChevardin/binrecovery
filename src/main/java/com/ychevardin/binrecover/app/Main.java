package com.ychevardin.binrecover.app;

import com.ychevardin.binrecover.app.contexts.ApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext.getInstance().start();
    }
}
